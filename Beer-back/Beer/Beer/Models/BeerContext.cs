﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Beer.Models
{
    public partial class beerContext : DbContext
    {
        public beerContext()
        {
        }

        public beerContext(DbContextOptions<beerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BeerType> BeerType { get; set; }
        public virtual DbSet<Beerorders> Beerorders { get; set; }
        public virtual DbSet<Beers> Beers { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BeerType>(entity =>
            {
                entity.HasKey(e => e.TypeId)
                    .HasName("PRIMARY");

                entity.ToTable("beer_type");

                entity.Property(e => e.TypeId)
                    .HasColumnName("Type_Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Beskrivelse)
                    .IsRequired()
                    .HasMaxLength(225)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(225)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Beerorders>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("beerorders");

                entity.HasIndex(e => e.BeerId)
                    .HasName("Beer_Order");

                entity.HasIndex(e => e.OrderId)
                    .HasName("Beer_Order_Order");

                entity.Property(e => e.BeerId).HasColumnType("int(11)");

                entity.Property(e => e.OrderId).HasColumnType("int(11)");

                entity.HasOne(d => d.Beer)
                    .WithMany()
                    .HasForeignKey(d => d.BeerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Beer_Order");

                entity.HasOne(d => d.Order)
                    .WithMany()
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Beer_Order_Order");
            });

            modelBuilder.Entity<Beers>(entity =>
            {
                entity.HasKey(e => e.BeerId)
                    .HasName("PRIMARY");

                entity.ToTable("beers");

                entity.HasIndex(e => e.TypeId)
                    .HasName("Beer_type");

                entity.Property(e => e.BeerId).HasColumnType("int(11)");

                entity.Property(e => e.Beskrivelse)
                    .IsRequired()
                    .HasMaxLength(222)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasColumnType("int(11)");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TypeId)
                    .HasColumnName("Type_Id")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Beers)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Beer_type");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.EmployeeId)
                    .HasName("PRIMARY");

                entity.ToTable("employees");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("Employee_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FirstName).HasColumnType("int(11)");

                entity.Property(e => e.LastName).HasColumnType("int(11)");

                entity.Property(e => e.Rang).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrderId)
                    .HasName("PRIMARY");

                entity.ToTable("orders");

                entity.HasIndex(e => e.EmployeeId)
                    .HasName("Employee");

                entity.Property(e => e.OrderId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasDefaultValueSql("'current_timestamp()'")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.EmployeeId).HasColumnType("int(11)");

                entity.Property(e => e.Paid)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("'NULL'");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Employee");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
