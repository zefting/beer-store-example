﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.Dto
{
    public class BeerTypeReadDto
    {
        public int TypeId { get; set; }
        public string Type { get; set; }
        
    }
}
