﻿using Beer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.Dto
{
    public class OrdersReadDto
    {
        public int OrderId { get; set; }
        public byte? Paid { get; set; }
        public int EmployeeId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public List<Beers> Beers { get; set;}

        public virtual Employees Employee { get; set; }
    }
}
