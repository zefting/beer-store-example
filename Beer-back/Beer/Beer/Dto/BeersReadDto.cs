﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.Dto
{
    public class BeersReadDto
    {
        public int BeerId { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }
        public int Price { get; set; }
        public string Beskrivelse { get; set; }
    }
}
