﻿using System;
using System.Collections.Generic;
using Beer.Models;


namespace Beer.Dto
{
    public partial class BeerordersDto
    {
        public int BeerId { get; set; }
        public int OrderId { get; set; }
        public List<int> BeerIds { get; set; }
        public virtual Beers Beer { get; set; }
        public virtual Orders Order { get; set; }
    }
}
