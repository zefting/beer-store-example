﻿using Beer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.Data
{
    public class SqlBeerRepo : IBeerRepo
    {
        private readonly beerContext _context;

        public SqlBeerRepo(beerContext context)
        {
            _context = context;
        }
        public IEnumerable<Beers> GetAllBeers()
        {
            return _context.Beers.ToList();
        }

        public Beers GetBeerById(int Beer_id)
        {
            return _context.Beers.FirstOrDefault(P => P.BeerId == Beer_id);
        }

        // BeerType 
         public IEnumerable<BeerType> GetAllBeertypes()
        {
            return _context.BeerType.ToList();
        }

        // Orders
        public IEnumerable<Beerorders> GetAllOrders()
        {
            return _context.Beerorders.ToList();
        }

        public bool SaveChanges()
        {
            return(_context.SaveChanges() >=0);
        }

        IEnumerable<Orders> IBeerRepo.GetAllOrders()
        {
            var orderItems = _context.Orders.ToList();

            return orderItems;
        }

        IEnumerable<Beerorders> IBeerRepo.GetAllBeerOrders()
        {
            var beerOrderItems = _context.Beerorders.ToList();

            return beerOrderItems;
        }

        public Orders GetOrderById(int orderId)
        {
            return _context.Orders.FirstOrDefault(P => P.OrderId == orderId);
        }

        public List<Beers> GetBeersByOrder(int orderId)
        {   
            var beerOrderItems = _context.Beerorders.ToList();
            var beers = new List<Beers>();

                foreach(var beerOrder in beerOrderItems) 
                {
                     if(beerOrder.OrderId == orderId) 
                     {
                        beers.Add(beerOrder.Beer);
                     }
                }
            
            return beers;
        }



        public void PlaceOrder(Orders orderModel)
        {
            if(orderModel == null)
            {
                throw new ArgumentNullException(nameof(orderModel));
            }
            _context.Orders.Add(orderModel);
        }
        public void PlaceOrderBeerRelation(List<Beerorders> beerOrderList)
        {
            if(beerOrderList == null)
            {
                throw new ArgumentNullException(nameof(beerOrderList));
            }
            _context.Beerorders.AddRange(beerOrderList);
        }
    }
}
