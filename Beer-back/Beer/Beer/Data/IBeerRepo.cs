﻿using Beer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Beer.Dto;
using System.Threading.Tasks;

namespace Beer.Data
{
    public interface IBeerRepo
    {
        // save changes
        bool SaveChanges();
        // Beers
        IEnumerable<Beers> GetAllBeers();
        Beers GetBeerById(int Beer_id);

        // Beertype
        IEnumerable<BeerType> GetAllBeertypes();

        // Beerorder
        IEnumerable<Beerorders> GetAllBeerOrders();

        // Orders
        IEnumerable<Orders> GetAllOrders();
        Orders GetOrderById(int orderId);
        List<Beers> GetBeersByOrder(int orderId);
        void PlaceOrderBeerRelation(List<Beerorders> beerOrderList);
        void PlaceOrder(Orders orderModel);
    }
}
