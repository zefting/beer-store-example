﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Beer.Data;
using Beer.Dto;
using Beer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Beer.Controllers
{

    [Route("api/orders")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IBeerRepo _repository;
        private readonly IMapper _mapper;

        public OrdersController(IBeerRepo repository, IMapper mapper)
            {
                _repository = repository;
                _mapper = mapper;
            }

        // GET api/orders
        [HttpGet]
        public ActionResult<IEnumerable<OrdersReadDto>> GetAllOrders()
        {
            var orderItems = _repository.GetAllOrders();
            var beerOrderItems = _repository.GetAllBeerOrders();
            var orderDtos = _mapper.Map<IEnumerable<OrdersReadDto>>(orderItems);
            
            foreach(var order in orderDtos)
                {
                var beers = new List<Beers>();
                    foreach(var beerOrder in beerOrderItems)
                    {
                        if(beerOrder.OrderId == order.OrderId)
                        {
                            beers.Add(beerOrder.Beer);
                        }
                    }

                order.Beers = beers;

                }
            return Ok(orderDtos);
        }

        // GET api/orders/5
        [HttpGet("{id}", Name ="GetOrderById")]
        public ActionResult<OrdersReadDto> GetOrderById(int orderId)
        {
            var orderItem = _repository.GetOrderById(orderId);
            var beers = _repository.GetBeersByOrder(orderId);

            if (orderItem != null)
            {   
                var mapOrder = _mapper.Map<OrdersReadDto>(orderItem);
                mapOrder.Beers = beers;
                return Ok(mapOrder);

            }
            return NotFound();
        }

        // Post /api/orders
        [HttpPost]
        public ActionResult<OrdersReadDto> PlaceOrder(OrderCreateDto orderCreateDto)
            {
                var orderModel = _mapper.Map<Orders>(orderCreateDto);
                _repository.PlaceOrder(orderModel);
                _repository.SaveChanges();
                
                var ordersReadDto = _mapper.Map<OrdersReadDto>(orderModel);

                var beerOrdersList = new List<Beerorders>();
                foreach(var beerId in orderCreateDto.BeerIds)
                {
                    var beerOrder = new Beerorders();
                    beerOrder.OrderId = ordersReadDto.OrderId;
                    beerOrder.BeerId = beerId;
                    beerOrdersList.Add(beerOrder);
                }
                _repository.PlaceOrderBeerRelation(beerOrdersList);
                _repository.SaveChanges();

                return CreatedAtRoute(nameof(GetOrderById), new { Id = ordersReadDto.OrderId }, ordersReadDto);
            }
    }

}
