﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Beer.Data;
using Beer.Dto;
using Beer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;



namespace Beer.Controllers
{   
    [Route("api/beers")]
    [ApiController]
    public class BeersController : ControllerBase
    {
        private readonly IBeerRepo _repository;
        private readonly IMapper _mapper;

        public BeersController(IBeerRepo repository, IMapper mapper)
            {
                _repository = repository;
                _mapper = mapper;
            }

        // GET api/beers
        [HttpGet]
        public ActionResult <IEnumerable<BeersReadDto>> GetAllBeers()
        {
            var beerItems = _repository.GetAllBeers();
            return Ok(_mapper.Map<IEnumerable<BeersReadDto>>(beerItems));
        }

        // GET api/beers/5
        [HttpGet("{id}")]
        public ActionResult <BeersReadDto> GetBeerById (int Beer_id)
        {
            var beerItem = _repository.GetBeerById(Beer_id);
            if(beerItem != null)
            {
                return Ok(_mapper.Map<BeersReadDto>(beerItem));
            }
            return NotFound();
        }
        // BeerType
        
        [HttpGet("beertype")]
        public ActionResult <IEnumerable<BeerTypeReadDto>> GetAllBeertypes()
        {
            var beerTypeItems = _repository.GetAllBeertypes();
            return Ok(_mapper.Map<IEnumerable<BeerTypeReadDto>>(beerTypeItems));
        }
    }
}
