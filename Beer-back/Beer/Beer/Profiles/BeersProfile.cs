﻿using AutoMapper;
using Beer.Dto;
using Beer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.Profiles
{
    public class BeersProfile : Profile
    {
        public BeersProfile()
        {
            CreateMap<Beers, BeersReadDto>();
            CreateMap<BeerType, BeerTypeReadDto>();
        }
    }
}
