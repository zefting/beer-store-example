﻿using Beer.Dto;
using Beer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace Beer.Profiles
{
    public class OrdersProfile : Profile
    {
        public OrdersProfile()
        {
            CreateMap<Orders, OrdersReadDto>();
            CreateMap<OrderCreateDto, Orders>();

        }
    }
}
