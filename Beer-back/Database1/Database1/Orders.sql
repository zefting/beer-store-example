﻿CREATE TABLE [dbo].[Table2]
(
	[Order_id] INT NOT NULL PRIMARY KEY, 
    [Beer_Orders] INT NOT NULL, 
    [Paid] TINYINT NULL, 
    [Employee_id] INT NOT NULL 
)
