﻿CREATE TABLE [dbo].[Table1]
(
	[Beer_id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Type] INT NOT NULL, 
    [Price] INT NOT NULL
)
